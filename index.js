let num1 = Number(prompt("Please input a number:"));
console.log(`The number you provided is ${num1}`);

while(num1>=50){
	if(num1 % 5 === 0 && num1 % 10 === 0){
		console.log (`The number ${num1} is divisible by 10. Skipping the number`);
	}
	else if (num1 % 5 === 0 && num1 % 10 !== 0){
		console.log(num1);
	}
	num1--;
	continue;
}


let string1 = prompt("Please input a string:").toLowerCase();
let result = ""

for (let i = 0; i < string1.length; i++){
	if(string1[i] === "a" || string1[i] === "e"|| string1[i] ==="i"|| string1[i] =="o"|| string1[i] ==="u"){
	    continue;
	} else {
		result = result + string1[i];
	}
}

console.log(`The string you provided is "${string1}" and the resulting string is: "${result}"`);
